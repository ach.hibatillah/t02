public class static1 {

    private String status; 
    private int volume; 
    private int brightness; 
    private String cable; 

    public void turnOff(){
        status = "Mati";
    }

    public void turnOn(){
        status = "Hidup";
    }

    public void Freeze(){
        status = "Freeze";
    }

    public void volumeUp(){
        volume++;
    }

    public void volumeDown(){
        volume--;
    }

    public void setVolume(int vol){
        volume = vol;
    }

    public void brightnessUp(){
        brightness++;
    }

    public void brightnessDown(){
        brightness--;
    }

    public void setBrightness(int bright){
        brightness = bright;
    }

    public void cableUp(){
        switch (cable) {
            case "VGA":
                cable = "DVI";
                break;
            case "DVI":
                cable = "HDMI";
                break;
            case "HDMI":
                cable = "DisplayPort";
                break;
            case "DisplayPort":
                cable = "VGA";
                break;
            default:
                cable = "VGA";
                break;
        }
    }

    public void cableDown(){
        switch (cable) {
            case "VGA":
                cable = "DisplayPort";
                break;
            case "DVI":
                cable = "VGA";
                break;
            case "HDMI":
                cable = "DVI";
                break;
            case "DisplayPort":
                cable = "HDMI";
                break;
            default:
                cable = "VGA";
                break;
        }
    }

    public void setCable(String cableType){
        cable = cableType;
    }

    public void displayStatus(){
        System.out.println("---------------LCD---------------");
        System.out.println("Status LCD saat ini      : " + status);
        System.out.println("Volume LCD saat ini      : " + volume);
        System.out.println("Brightness LCD saat ini  : " + brightness);
        System.out.println("Cable yang digunakan     : " + cable);
    }
}
